#!/usr/bin/env python

import sys
import csv
import json
import hashlib
import random
import time

BTC_BLOCK_511397_HASH = "0000000000000000004db6845500d6e90b4e5d8960c9e5adbb008da4f3d97501"

def main(argv):
    duplicate_checker_list = [] 
    original_list = {}
    new_list = {}
    with open(argv[0]) as f:
        for line in csv.DictReader(f, fieldnames=('Email Address', 'BNB Address', 'Telegram ID', 'ELA Address')):
            email_address = line['Email Address'].strip().lower()
            telegram_id = line['Telegram ID'].strip()
            bnb_address = line['BNB Address'].strip()
            ela_address = line['ELA Address'].strip()
            original_list[email_address] = "{0},{1},{2}".format(email_address, telegram_id, ela_address)
            telegram_id = telegram_id.lower()
            if email_address not in duplicate_checker_list and bnb_address not in duplicate_checker_list and ela_address not in duplicate_checker_list and telegram_id not in duplicate_checker_list:
                duplicate_checker_list.append(email_address)
		duplicate_checker_list.append(bnb_address)
                duplicate_checker_list.append(ela_address)
                duplicate_checker_list.append(telegram_id)
                new_list[email_address] = "{0},{1},{2}".format(email_address, telegram_id, ela_address)
    
    del duplicate_checker_list
  
    random.seed(BTC_BLOCK_511397_HASH)
    new_list_keys = new_list.keys() 
    with open(argv[1], 'wb') as out:
        num_winners = 1
        out.write("{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "200 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 2
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "100 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 4
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "50 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 10
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "10 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 20
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "2 ELA", num_winners):
            out.write("{0}\n".format(winner))
        num_winners = 1000
        out.write("\n{0} Lucky Winners:\n".format(num_winners))
        for winner in select_winners(new_list, original_list, new_list_keys, "0.5 ELA", num_winners):
            out.write("{0}\n".format(winner))
 
def select_winners(new_list, original_list, new_list_keys, winner_type, num_winners):
    """
	This ensures that for very short periods of time, the inital seeds for feeding the 
  	pseudo-random generator will be hugely different between two successive calls
    """
    result = []
    for i in range(num_winners):
        winner = random.choice(new_list_keys)
        result.append(original_list[winner])
        new_list_keys.remove(winner)
        del new_list[winner]
    return result

if __name__ == '__main__':
    main(sys.argv[1:])
